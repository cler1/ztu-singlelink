<?php
if (!file_exists("admin/data/initialized")) {
	if (is_writable("admin/data") && is_writable(".")) {
		header("Location: admin/");
		echo "Please go to admin/";
	}
	else {
		echo "ztu-singlelink is not initialized, but the permissions are not set correctly. ";
		echo "Please allow your web server to write in this directory and all subdirectories.";
	}

	exit();
}
?>
<?php
ob_start();
session_start();

$logged_in = isset($_SESSION["logged_in"]);
$action = "login";

if (!file_exists("data/initialized")) {
	$action = "setup";
}
elseif (!empty($_GET["action"])) {
	$action = $_GET["action"];
}

function err($msg) {
	echo "<p><b>ERROR:</b> $msg</p>";
}

function ok($msg) {
	echo "<p>$msg</p>";
}

function menu() {
	include "html/menu.php";
}
?>
<!doctype html>
<html>
<head>
<title>ztu-singlelink</title>
</head>
<body>
<h1>This page intentionally left blank</h1>

<p><a href="admin/">Go to admin page</a></p>

<p><a href="https://gitlab.com/cler1/ztu-singlelink">ztu-singlelink</a> is &copy; <a href="https://max.pm">Max R. P. Grossmann</a>, 2020. <a href="LICENSE" target="_new">Licensed under the MIT license</a>.</p>
</body>
</html>