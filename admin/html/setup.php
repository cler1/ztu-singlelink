<h2>Setup</h2>

<?php
if ($logged_in) {
	menu();
}

if (isset($_POST["pw1"])) {
	if ($_POST["pw1"] == $_POST["pw2"]) {
		if (strlen($_POST["pw1"]) >= 4) {
			if (@touch("data/initialized")) {
				$w = @file_put_contents("data/password", password_hash($_POST["pw1"], PASSWORD_DEFAULT));
				
				if ($w !== FALSE) {
					ok("New password successfully set. Please <a href='?action=login'>log in</a>.");
					$noform = true;
				}
				else {
					err("Could not write to admin/data/password. Please set file permissions accordingly.");
					@unlink("data/initialized");
				}
			}
			else {
				err("Could not write to admin/data/initialized. Please set file permissions accordingly.");
			}
		}
		else {
			err("Password too short. Please enter at least 4 characters.");
		}
	}
	else {
		err("Passwords do not match.");
	}
}

if (!isset($noform)) {
?>
<p>Please set the login password.</p>

<form action="?action=setup" method="post">
	<p>New password: <input type="password" name="pw1" /></p>
	<p>Once again: <input type="password" name="pw2" /></p>
	<p><input type="submit" /></p>
</form>
<?php
}
?>