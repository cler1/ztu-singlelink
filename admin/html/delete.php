<h2>Monitor sessions</h2>

<?php
menu();

function lines($file) {
	return count(file($file));
}

if (isset($_POST["submit"]) && isset($_POST["delete"]) && is_array($_POST["delete"])) {
	foreach ($_POST["delete"] as $ds) {
		if (!file_exists("data/urls_$ds")) {
			err("Invalid session specified (<i>$ds</i>).");
			continue;
		}

		if (unlink("../$ds/index.php") && rmdir("../$ds")) {
			ok("Session <i>$ds</i> deleted. <a href='data:text/plain;base64,".base64_encode(file_get_contents("data/urls_$ds"))."' download='urls-$ds.txt'>Click here to download the URLs of the deleted session.</a>");
		}
		else {
			ok("Session <i>$ds</i> could not be deleted.");
		}
	}
}
?>

<p>The following sessions are live:</p>

<form action="?action=delete" method="post">
<table border="1">
<tr><th>Delete?</th><th>Name</th><th>Live since</th><th>Participation link</th><th>URLs taken</th><th>URLs remaining</th><th>Total URLs</th><th>Download all URLs</th></tr>
<?php
$sess = glob("data/urls_$sessionname_prefix*");

foreach ($sess as $f) {
	$s = str_replace("data/urls_", "", $f);

	if (file_exists("../$s/index.php")) {
		$u = lines($f);
		$taken = (file_exists("data/taken_$s")) ? (int)file_get_contents("data/taken_$s") : 0;
		
		echo "<tr>
			<td><center><input type='checkbox' name='delete[]' value='$s' /></center></td>
			<td><center><b>$s</b></center></td>
			<td>".date("Y-m-d H:i-s", filemtime("../$s"))."</td>
			<td>".linknoclick("../$s", "(Right-click here)")."</td>
			<td>$taken</td>
			<td>".($u-$taken)."</td>
			<td>$u</td>
			<td><a href='data:text/plain;base64,".base64_encode(file_get_contents($f))."' download='urls-$s.txt'>Click here</a></td>
		</tr>";
	}
}
?>
</table>

<p><input type="submit" name="submit" /></p>
</form>
