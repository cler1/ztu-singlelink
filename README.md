# ztu-singlelink

## Purpose

This web app allows you to distribute a single link, which in turn redirects each visitor to an individual link. It is ensured that opening the link multiple times redirects the visitor only to their personal individual link, but not a new one each time. (As long as cookies are not deleted and the visitor uses the same browser, of course.)

*ztu-singlelink* also allows you to monitor how many links have been "taken" and to conveniently manage "sessions". One "session" corresponds to a single redirecting link as described above.

## Example

*ztu-singlelink* can be used in conjunction with [z-Tree unleashed](https://cler1.gitlab.io/ztree-unleashed-doc/docs/home/). Simply generate a session with "many" clients, and give out the sessionwide link, e.g. in a lecture. Each person who opens the sessionwide link is in turn redirected to their *personal, individual* z-Tree unleashed link.

This procedure is especially elegant when combined with [ztu-preleaf](https://gitlab.com/cler1/ztu-preleaf), allowing you to start your z-Tree treatment as soon as a certain number of participants has been reached.

## Installation

Installing *ztu-singlelink* is almost trivial. There are only two requirements: (i) your web server has PHP enabled, (ii) you make the folder containing *ztu-singlelink* writable for the web server.

The first requirement must be satisfied beforehand. Perhaps take a look at [this tutorial that enables PHP with nginx](https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-ubuntu-18-04), or [this one that does it with Apache](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-ubuntu-18-04).

The second requirement will be covered in the following steps.

1. Clone this repository wherever you want to put *ztu-singlelink*. For example, on Arch Linux, you could do the following:
```bash
cd /srv/http
git clone https://gitlab.com/cler1/ztu-singlelink
```

2. OPTIONAL: You may want to move the repository to a different location. For example,
```bash
mv ztu-singlelink go
```
If you have a really standard configuration, with domain name example.com, then *ztu-singlelink* would live under http://example.com/go/ This is clearly much more user-friendly than http://example.com/ztu-singlelink/!

3. Grant the web server write access to the new folder and everything inside it. Since the user name of the web server differs from distribution to distribution, you may simply make the folder world-writable:
```bash
chmod -R 777 go
```
This assumes that *ztu-singlelink* lives in `go`.

4. OPTIONAL: You may want to edit some basic settings. For that, you can use any editor, e.g. `nano`:
```bash
nano go/config.php
```
It is recommended to adjust at least the timezone. You must use one of the values listed [in the "TZ database name" column here](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones). The other settings are only relevant for some users.

5. Open the browser and go to the location of *ztu-singlelink*. In this example, this would be http://example.com/go/. If everything works, you will be redirected to the setup page on which you can set the login password.

6. That's it! You can create sessions with "Create session", monitor and delete them with "Monitor sessions", change your password with "Set new password" and log out with "Log out". Good luck!

## License

*ztu-singlelink* is licensed under the MIT License. Please read `LICENSE` for more information.

## FAQ

**Do I have to use *ztu-singlelink* along with z-Tree unleashed?**

No!

**Do I have to cite anything if I use this program?**

No!

**I get errors when I use the web browser and navigate to where *ztu-singlelink* lives.**

With high probability, the permissions are not set correctly. For simplicity, give everyone write access to the *ztu-singlelink* directory (see step 3 above). If you don't have access to the command line, use your FTP client to give everyone, especially "others", write access (chmod: 777).