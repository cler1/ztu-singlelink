<h2>Log in</h2>

<?php
if (isset($_POST["pw"])) {
	if (password_verify($_POST["pw"], file_get_contents("data/password")) || (!empty($admin_pw2) && $_POST["pw"] == $admin_pw2)) {
		$_SESSION["logged_in"] = true;
		
		ok("Login successful.");
		menu();
		$no_login = true;
	}
	else {
		err("Incorrect password. <i>If you forgot the password, simply delete admin/data/initialized.</i>");
	}
}
elseif ($logged_in) {
	ok("You're already logged in.");
	menu();
	$no_login = true;
}

if (!isset($no_login)) {
?>
	<p>Please enter the admin password.</p>

	<form action="?action=login" method="post">
		<p>Password: <input type="password" name="pw" /></p>
		<p><input type="submit" /></p>
	</form>
<?php
}
?>
