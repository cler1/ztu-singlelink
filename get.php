<?php
include "config.php";

usleep(mt_rand(0, round($max_delay*1e6))); // add some random delay (helps in physical labs)

$session = explode("/", $_SERVER["SCRIPT_FILENAME"]);
$session = $session[count($session)-2];

if (file_exists("../admin/data/urls_$session")) {
	$urls = array_map("trim", file("../admin/data/urls_$session"));
	$dest = "http://example.com";

	if (!empty($_COOKIE["destination"]) && in_array($_COOKIE["destination"], $urls)) {
		$dest = $_COOKIE["destination"];
	}
	else {
		$n_taken = (int)file_get_contents("../admin/data/taken_$session");

		if (!empty($urls[$n_taken])) {
			$dest = trim($urls[$n_taken]);
			setcookie("destination", $dest, time() + $validity);

			file_put_contents("../admin/data/taken_$session", $n_taken+1, LOCK_EX);
		}
		else {
			echo $out_of_links;
			exit();
		}
	}

	header("Location: $dest");
	echo "Please go to $dest";
}
else {
	echo $session_invalid;
}
?>
