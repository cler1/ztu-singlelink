<?php
include "../config.php";

date_default_timezone_set($timezone); // you may want to set that to your timezone

ob_start();
session_start();

$logged_in = isset($_SESSION["logged_in"]);
$action = "login";

if (!file_exists("data/initialized")) {
	$action = "setup";
}
elseif (!empty($_GET["action"])) {
	$action = $_GET["action"];
}

function err($msg) {
	echo "<p><b>ERROR:</b> $msg</p>";
}

function ok($msg) {
	echo "<p>$msg</p>";
}

function linknoclick($location, $text) {
	return "<a href=\"$location\" onclick=\"alert('Nope! RIGHT-click, not click, to copy the sessionwide link.'); return false\">$text</a>";
}

function menu() {
	include "html/menu.php";
}
?>
<!doctype html>
<html>
<head>
<title>ztu-singlelink</title>
</head>
<body>
	<h1>ztu-singlelink</h1>
	<?php
	if ($action == "login") {
		include "html/login.php";
	}
	elseif ($action == "setup" && ($logged_in || !file_exists("data/initialized"))) {
		include "html/setup.php";
	}
	elseif ($action == "create" && $logged_in) {
		include "html/create.php";
	}
	elseif ($action == "delete" && $logged_in) {
		include "html/delete.php";
	}
	elseif ($action == "logout" && $logged_in) {
		include "html/logout.php";
	}
	else {
	?>
		<p>Invalid action. <a href="?action=login">Log in</a>.</p>
	<?php
	}
	?>
<hr />
<p><a href="https://gitlab.com/cler1/ztu-singlelink">ztu-singlelink</a> is &copy; <a href="mailto:ztusinglelink@max.pm">Max R. P. Grossmann</a>, 2020. <a href="../LICENSE" target="_new">Licensed under the MIT license</a>.</p>
<?php
if ($logged_in) {
	if (file_exists("../.git/refs/heads/master")) {
		echo "Version: ";
		readfile("../.git/refs/heads/master");
	}
}
?>
</body>
</html>
<?php
ob_end_flush();
?>