<h2>Create session</h2>

<?php
menu();

do {
	$suggest = $sessionname_prefix;
	
	for ($i = 0; $i < $namelen; $i++) {
		$suggest .= $alphabet[mt_rand(0, strlen($alphabet)-1)];
	}
} while (file_exists("../$suggest/index.php") || file_exists("data/urls_$suggest"));

if (isset($_POST["submit"])) {
	$s = ($allow_custom_sessionnames) ? trim($_POST["session"]) : $suggest;
	
	// processing the URLs
	
	$urls_good = array();
	
	foreach (explode("\n", $_POST["urls"]) as $ui) {
		$sanitized_url = trim($ui);
		
		if (substr($sanitized_url, 0, 4) == "http") {
			if (!isset($_POST["duplicate"])) {
				if (in_array($sanitized_url, $urls_good)) {
					continue;
				}
			}
			
			$urls_good[] = $sanitized_url;
		}
	}
	
	if (!isset($_POST["no_rand"]) && (int)$_POST["taken"] == 0) {
		shuffle($urls_good);
	}
	
	$out = implode("\n", $urls_good);
	$taken = (isset($_POST["taken"]) && is_numeric($_POST["taken"])) ? (string)(int)$_POST["taken"] : "0";
	
	if (file_exists("../$s")) {
		@unlink("../$s/index.php");
		@rmdir("../$s");
	}

	if (file_put_contents("data/urls_$s", $out) !== FALSE &&
		file_put_contents("data/taken_$s", $taken) !== FALSE &&
		mkdir("../$s") &&
		symlink("../get.php", "../$s/index.php")) {
			ok("The session <i>$s</i> was successfully created with ".count($urls_good)." target URLs (".((isset($_POST["duplicate"])) ? 'duplicates allowed' : 'de-duplicated').", $taken assumed to be taken). ".linknoclick("../$s", "Right-click here to copy the sessionwide link to be distributed."));
	}
	else {
		err("The session could not be created. Please check the file permissions.");
	}
}
?>

<form action="?action=create" method="post">
<p>Please enter the target URLs here (one line per URL):</p>

<textarea style="width: 700px; height: 300px" name="urls"></textarea>

<p>Do <b><u>NOT</u></b> include the experimenter's link.</p>

<?php
if ($allow_custom_sessionnames) {
?>
<p>Please specify the name of the session. This name will be included in the sessionwide link:<br />
<input type="text" name="session" value="<?=$suggest ?>" /><br />
<i>An available session name was automatically generated. If you specify your own name, existing sessions with that name will be overwritten. Do not include anything but alphanumeric characters.</i>
</p>
<?php
}
else {
	echo "<p>A session name will be automatically generated.</p>";
}
?>

<p><input type="checkbox" name="duplicate" value="yes" /> Allow duplicate target URLs</p>

<p><input type="checkbox" name="no_rand" value="yes" /> Do not randomize target URLs</p>

<p>Assume that <input type="text" name="taken" size="3" value="0" /> URLs are already taken (useful for recovery, implies disabling randomization)</p>

<p><input type="submit" name="submit" /></p>
</form>